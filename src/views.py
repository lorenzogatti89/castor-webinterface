#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""

# Import section (built-in modules|third-party modules)
import os
import socket
import subprocess
from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from subprocess import Popen, PIPE, TimeoutExpired
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.files.storage import default_storage
import uuid
import pickle
from .backend import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

# Authorship information

__project__ = 'src'
__product__ = 'views.py'
__editor__ = 'PyCharm'
__author__ = 'lorenzogatti'
__copyright__ = "Copyright 2018, Lorenzo Gatti"
__credits__ = ["Lorenzo Gatti"]
__license__ = "GPL"
__date__ = '21.03.18'
__version__ = "1.0"
__maintainer__ = "Lorenzo Gatti"
__email__ = "lg@lorenzogatti.me"
__status__ = "Development"

# Main code

PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates/'
        ],
    },
]


def version__castor():
    process = Popen([settings.EXECPATHS["path_CastorStatic"], " version=true"], stdout=PIPE, universal_newlines=True)
    softwareInfo = dict()
    i = 0
    for line in iter(process.stdout.readline, ''):
        line.rstrip()

        if i == 0:
            softwareInfo['name'] = line.split()[0]
            softwareInfo['version'] = line.split()[1]
        if i == 1:
            temp = line.split()[0]
            softwareInfo['branch'] = temp.split('/')[2]
            softwareInfo['commit'] = line.split()[1][0:7]
        if i == 2:
            softwareInfo['date'] = line.split(',')[0]
            softwareInfo['time'] = line.split(',')[1]

        i += 1

    return softwareInfo


def version__castor_web():
    result = subprocess.check_output("/usr/bin/git log -1 --date=short --pretty=\'format:%D(%h) build %cd\'",
                                     shell=True,
                                     universal_newlines=True)
    return result


SOFTWARE_INFO = version__castor()
VERSION__CASTOR = SOFTWARE_INFO["name"] + " " + SOFTWARE_INFO["version"]
BUILD__CASTOR = "HEAD -> " + SOFTWARE_INFO["branch"] + ", origin/" + SOFTWARE_INFO["branch"] + ", origin/HEAD(" + SOFTWARE_INFO["commit"] + ") build " + SOFTWARE_INFO["date"]
VERSION_WEBSERVER = version__castor_web()

"""
Entry point for index page
"""


def get_dns_domain():
    return socket.getfqdn().split('.', 1)[1]


# @login_required
def index(request):
    return HttpResponseRedirect("/entry/")


def docs(request):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    html = render_to_string('features.html', {'title': VERSION__CASTOR,
                                              'version': BUILD__CASTOR,
                                              'version_webserver': VERSION_WEBSERVER,
                                              'server_load': PENDINGS_ANALYSES})

    return HttpResponse(html)


def examples(request):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    outs = ""
    html = render_to_string('examples.html', {'title': VERSION__CASTOR,
                                              'version': BUILD__CASTOR,
                                              'version_webserver': VERSION_WEBSERVER,
                                              'server_load': PENDINGS_ANALYSES,
                                              'div_content': outs})

    return HttpResponse(html)


def about(request):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    outs = ""
    html = render_to_string('examples.html', {'title': VERSION__CASTOR,
                                              'version': BUILD__CASTOR,
                                              'version_webserver': VERSION_WEBSERVER,
                                              'server_load': PENDINGS_ANALYSES,
                                              'div_content': outs})

    return HttpResponse(html)


def file_upload(filefield, uniqueID, request):
    save_path = str(settings.MEDIA_ROOT) + "/" + str(uniqueID) + "/" + str(request.FILES.get(filefield))
    path = default_storage.save(save_path, request.FILES.get(filefield))
    return default_storage.path(path)


def parsingPOST_AnalysisForm(request, uniqueID):
    varfields = {"inputdata_alphabet",
                 "alignment_execute",
                 "alignment_profiler_filename",
                 "sites_settoconsider",
                 "sites_removestopcodons",
                 "site_gapsproportion",
                 "site_unresolvedproportion",
                 "sites_manualselection",
                 "inittree_method",
                 "inittree_method_distance_alg",
                 "inittree_method_random_bl",
                 "inittree_method_user_filename",
                 "distancematrix_execute",
                 "inputdata_distmatrix_filename",
                 "submodel_frequencyset_desc",
                 "submodel_asvr_desc",
                 "submodel_model_desc",
                 "numopt_method",
                 "numopt_method_bl",
                 "numopt_method_final",
                 "opt_reparametrization",
                 "opt_eval_likelihoodprecision",
                 "opt_eval_maxevaluations",
                 "opt_parameter_constraints",
                 "opt_parameter_ignore",
                 "optimization_profiler_filename",
                 "optimization_messagehandler_filename",
                 "opt_topology_execute",
                 "opt_topology_algorithm_desc",
                 "opt_topology_rearrangments",
                 "opttop_maxiterations",
                 "opt_topology_algo_hillclimbing_startnodes",
                 "opttop_method_bl",
                 "optalig_algorithm_execute",
                 "optalig_algorithm_method",
                 "optalig_algorithm_stop",
                 "opt_eval_tolerance",
                 "opt_eval_maxeval",
                 "output_tree_filename",
                 "output_format_tree",
                 "output_alignment_filename",
                 "output_format_alignment",
                 "output_estimates_filename",
                 "output_info_filename",
                 "output_package_execute"
                 }

    filefields = {"inputdata_filename",
                  "inittree_method_user_filename",
                  "inputdata_distmatrix_filename"
                  }

    map = dict()

    for field in varfields:
        map[field] = request.POST.get(field)
        print("%s=%s" % (field, map[field]))

    os.mkdir(settings.MEDIA_ROOT + "/" + str(uniqueID));

    for field in filefields:

        if (request.FILES.get(field)):
            file_upload(field, uniqueID, request)

        map[field] = request.FILES.get(field)
        print("%s=%s" % (field, map[field]))

    return map


# @login_required
@csrf_exempt
def InputUploadView(request):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    # tab_section = int(request.GET.get('tab', ''))
    if hasattr(request, 'user'):
        if (request.user.username):
            print('user logged in with name %s' % request.user.username)
        else:
            print('no user logged in')

    # if this is a POST request we need to process the form data
    # if request.method == 'POST':
    #     # create a unique id to identify the session
    #     my_id = uuid.uuid1()
    #
    #     # create a form instance and populate it with data from the request:
    #     map = parsingPOST_AnalysisForm(request, my_id)
    #
    #     session_path = settings.MEDIA_ROOT + "/" + str(my_id)
    #
    #     # create the parameter file
    #     compileParameterFile(session_path, map)
    #
    #     save_path = session_path + "/param.pkl"
    #     f = open(save_path, "wb")
    #     pickle.dump(map, f)
    #     f.close()
    #
    #     # submit the analysis
    #     pid = submitAnalysisToExecutor(session_path)
    #
    #     # store analysis uuid
    #     addAnalysis(my_id, pid, request.user.username, socket.getfqdn())
    #
    #     # redirect to a new URL:
    #     return HttpResponseRedirect('/proc/' + str(my_id) + '/')

    return render(request, 'form.html', {'title': VERSION__CASTOR,
                                         'version': BUILD__CASTOR,
                                         'version_webserver': VERSION_WEBSERVER,
                                         'server_load': PENDINGS_ANALYSES})


@csrf_exempt
def submitProcess_backend(request):
    # tab_section = int(request.GET.get('tab', ''))
    if hasattr(request, 'user'):
        if (request.user.username):
            print('user logged in with name %s' % request.user.username)
        else:
            print('no user logged in')

    # if this is a POST request we need to process the form data
    # if request.method == 'POST':

    # create a unique id to identify the session
    my_id = uuid.uuid1()

    # create a form instance and populate it with data from the request:
    map = parsingPOST_AnalysisForm(request, my_id)

    session_path = settings.MEDIA_ROOT + "/" + str(my_id)

    # create the parameter file
    compileParameterFile(session_path, map)

    save_path = session_path + "/param.pkl"
    f = open(save_path, "wb")
    pickle.dump(map, f)
    f.close()

    # submit the analysis
    pid = submitAnalysisToExecutor(session_path)

    # store analysis uuid
    addAnalysis(my_id, pid, request.user.username, socket.getfqdn())

    return my_id


@csrf_exempt
def submitProcessBatch(request):
    my_id = submitProcess_backend(request)
    # redirect to a new URL:
    return render(request, 'exit.html')


def execClearProcessesPerUser(request):
    clearUserCompletedProcesses(request)
    # redirect to a new URL:
    return render(request, 'exit.html')


@csrf_exempt
def submitProcess(request):
    my_id = submitProcess_backend(request)
    # redirect to a new URL:
    return HttpResponseRedirect('/proc/' + str(my_id) + '/')


def WaitingForResultsView(request, session_id):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    save_path = settings.MEDIA_ROOT + "/" + str(session_id)

    with open(save_path + "/param.pkl", 'rb') as f:
        data = pickle.load(f)

    inputData_alphabet = data['inputdata_alphabet']
    inputData_file = data['inputdata_filename']
    inputTree = data['inittree_method']
    exec_alignment = data['alignment_execute']
    exec_optimisation_tree = data['opt_topology_execute']
    exec_optimisation_alig = data['optalig_algorithm_execute']

    # get the output of the execution
    try:
        F = open(save_path + "/stdout.txt", 'r')
        filecontent = F.read()
    except FileNotFoundError:
        filecontent = "File not found: " + save_path + "/stdout.txt"

    # find end of the file
    exec_completed = False
    command = "grep -c \"Total execution time:\" " + save_path + "/stdout.txt"

    process = Popen(command, shell=True, stdout=PIPE)

    try:
        outs, errs = process.communicate(timeout=15)
        if (int(outs) == 1):
            exec_completed = True
    except TimeoutExpired:
        process.kill()

    # find if the analysis was terminated by an error
    error = False
    pid = int(getPIDfromUUID(session_id))
    if (isProcessRunning(pid) != True):
        error = True
        updateAnalysis(session_id, 1)

    if (exec_completed != True):
        errorMessage = ""
        if (error == True):
            errorMessage = "An error has occurred during the execution of your analysis. Please check the log file and resubmit!"

        return render(request, 'process.html', {'title': VERSION__CASTOR,
                                                'version': BUILD__CASTOR,
                                                'version_webserver': VERSION_WEBSERVER,
                                                'server_load': PENDINGS_ANALYSES,
                                                'inputData_alphabet': inputData_alphabet,
                                                'inputData_file': inputData_file,
                                                'inputTree': inputTree,
                                                'exec_alignment': exec_alignment,
                                                'exec_optimisation_tree': exec_optimisation_tree,
                                                'exec_optimisation_alig': exec_optimisation_alig,
                                                'exec_stdout': filecontent,
                                                'session_id': session_id,
                                                'errorMessage': errorMessage
                                                })
    else:

        # render the results in a png file
        image_path = plotResults(save_path, session_id, data)

        # estimate read
        filecontent = ""
        try:
            # F = open(save_path+"/best_estimates.log",'r')
            # filecontent = F.read()
            # filecontent.strip()

            with open(save_path + "/best_estimates.log", 'r') as f:
                for line in f:
                    cleanedLine = line.strip()
                    filecontent += "\n" + cleanedLine

        except FileNotFoundError:
            filecontent = "File not found: " + save_path + "/best_estimates.txt"

        # prepare analysis package
        urlpackage = makeAnalysisPackage(settings.MEDIA_ROOT, str(session_id))

        ## mark uuid as completed
        updateAnalysis(session_id, 0)

        return render(request, 'results.html', {'title': VERSION__CASTOR,
                                                'version': BUILD__CASTOR,
                                                'version_webserver': VERSION_WEBSERVER,
                                                'server_load': PENDINGS_ANALYSES,
                                                'image_path': image_path,
                                                'exec_estimates': filecontent,
                                                'package_url': urlpackage})


@login_required
def listExecutionsPerUser(request):
    PENDINGS_ANALYSES = currentPendingAnalyses()

    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])
    sql = "SELECT * FROM sessions WHERE username=\"" + request.user.username + "\"";

    records = ""

    try:
        cur = conn.cursor()
        cur.execute(sql)
        records = cur.fetchall()

        for record in records:
            pid = int(getPIDfromUUID(record[0]))
            if (isProcessRunning(pid) != True):
                error = True
                updateAnalysis(record[0], 1)

        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')

    return render(request, 'exec_list.html', {'title': VERSION__CASTOR,
                                              'version': BUILD__CASTOR,
                                              'version_webserver': VERSION_WEBSERVER,
                                              'server_load': PENDINGS_ANALYSES,
                                              'records': records})
