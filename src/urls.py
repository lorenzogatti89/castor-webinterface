"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('examples/', views.examples, name='examples'),
    path('docs/', views.docs, name='docs'),
    path('about/', views.about, name='about'),
    path('entry/', views.InputUploadView, name='entry'),
    path('submit/', views.submitProcess, name='submit'),
    path('bsubmit/', views.submitProcessBatch, name='submitbatch'),
    path('proc/<uuid:session_id>/', views.WaitingForResultsView, name='processing'),
    path('kill/<uuid:session_id>/', views.killRunningSession, name='kill'),
    path('pack/<uuid:session_id>/', views.packAnalysis, name='pack'),
    path('clearuserprocesses/', views.execClearProcessesPerUser, name='clearuserprocesses'),
    # path('login/', auth_views.login, {'template_name': 'login.html'}, name='login'),
    path('login/', auth_views.LoginView.as_view(template_name="login.html"), name="login"),
    path('logout/', auth_views.LogoutView, name='logout'),
    path('user/', views.listExecutionsPerUser, name='userpage'),
    path('admin/', admin.site.urls),

]
