Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/999d2566-37f8-11e8-842e-0a3c5e60f5b6/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master 7b8a88927fb4e9cc37355b718ef068adc03efb78 on date: 27 Mar 2018, 12:32:51                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: DNA
Allow gaps as extra character..........: yes

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 13

[Preparing initial tree]
Initial tree...........................: distance
Initial tree method....................: LZ compression
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/999d2566-37f8-11e8-842e-0a3c5e60f5b6/best_tree.nwk
Output tree format ....................: Newick
Branch lengths.........................: Input

[Setting up substitution model]
Substitution model.....................: L95+PIP
PIP.L95.alpha..........................: 0.5
PIP.L95.beta...........................: 0.5
PIP.L95.gamma..........................: 0.5
PIP.L95.kappa..........................: 1
PIP.L95.theta..........................: 0.5
PIP.lambda.............................: 0.1
PIP.mu.................................: 0.2
WARNING!!! Parameter rate_distribution not specified. Default used instead: Constant()
Distribution...........................: Constant
Number of classes......................: 1
- Category 0 (Pr = 1) rate.............: 1
Rate distribution......................: Constant
Number of classes......................: 1

[Computing the multi-sequence alignment]
[>                                     ]   4%[>>>                                   ]   8%[>>>>                                  ]  12%[>>>>>>                                ]  16%[>>>>>>>                               ]  20%[>>>>>>>>>                             ]  24%[>>>>>>>>>>                            ]  28%[>>>>>>>>>>>>                          ]  32%[>>>>>>>>>>>>>                         ]  36%[>>>>>>>>>>>>>>>                       ]  40%[>>>>>>>>>>>>>>>>                      ]  44%[>>>>>>>>>>>>>>>>>>                    ]  48%[>>>>>>>>>>>>>>>>>>>                   ]  52%[>>>>>>>>>>>>>>>>>>>>>                 ]  56%[>>>>>>>>>>>>>>>>>>>>>>                ]  60%[>>>>>>>>>>>>>>>>>>>>>>>>              ]  64%[>>>>>>>>>>>>>>>>>>>>>>>>>             ]  68%ERROR mu * T is too small