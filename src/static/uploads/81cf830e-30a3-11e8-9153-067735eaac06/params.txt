alphabet=Protein
input.sequence.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/notaligned_real_gp120small_aa.fa
alignment=true
input.sequence.sites_to_use=all
init.tree=distance
init.distance.method=bionj
init.distance.matrix.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/dm.txt
model=PIP(model=JC69, lambda=0.1, mu=0.2)
optimization=ND-Brent(derivatives=Newton)
optimization.final=bfgs
optimization.topology=true
optimization.topology.algorithm=greedy
optimization.topology.algorithm.operations=nni-search
optimization.topology.brlen_optimization=Brent
output.tree.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/best_tree.nwk
output.msa.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/best_alignment.fa
output.estimates=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/best_estimates.log
output.infos=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/81cf830e-30a3-11e8-9153-067735eaac06/info.log
