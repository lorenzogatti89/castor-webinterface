Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/2514d9cc-30a1-11e8-b01a-067735eaac06/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master d40f03e6bfa23c7a9677f9185428391e71f66256 on date: 25 Mar 2018, 21:57:44                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: DNA
Allow gaps as extra character..........: yes

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 13

[Preparing initial tree]
Initial tree...........................: user
You must specify a file for this parameter: input.tree.file
