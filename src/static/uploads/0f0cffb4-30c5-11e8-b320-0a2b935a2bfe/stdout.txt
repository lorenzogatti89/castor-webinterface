Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/0f0cffb4-30c5-11e8-b320-0a2b935a2bfe/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master bd8cbf58b22d6100c7751f18d9c016a708ab027e on date: 26 Mar 2018, 03:07:00                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: Proteic
Allow gaps as extra character..........: yes

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 23

[Preparing initial tree]
Initial tree...........................: distance
Initial tree method....................: LZ compression
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/0f0cffb4-30c5-11e8-b320-0a2b935a2bfe/best_tree.nwk
Output tree format ....................: Newick
Branch lengths.........................: Input

[Setting up substitution model]
Substitution model.....................: JC69+PIP
PIP.lambda.............................: 0.1
PIP.mu.................................: 0.2
WARNING!!! Parameter rate_distribution not specified. Default used instead: Constant()
Distribution...........................: Constant
Number of classes......................: 1
- Category 0 (Pr = 1) rate.............: 1
Rate distribution......................: Constant
Number of classes......................: 1

[Computing the multi-sequence alignment]
[>                                     ]   4%[>>>                                   ]   8%[>>>>>                                 ]  13%[>>>>>>                                ]  17%[>>>>>>>>                              ]  22%[>>>>>>>>>>                            ]  26%[>>>>>>>>>>>                           ]  31%[>>>>>>>>>>>>>                         ]  35%[>>>>>>>>>>>>>>>                       ]  40%[>>>>>>>>>>>>>>>>                      ]  44%[>>>>>>>>>>>>>>>>>>                    ]  48%[>>>>>>>>>>>>>>>>>>>>                  ]  53%[>>>>>>>>>>>>>>>>>>>>>                 ]  57%[>>>>>>>>>>>>>>>>>>>>>>>               ]  62%[>>>>>>>>>>>>>>>>>>>>>>>>>             ]  66%[>>>>>>>>>>>>>>>>>>>>>>>>>>>           ]  71%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>          ]  75%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        ]  80%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>      ]  84%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     ]  88%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   ]  93%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]  97%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] 100%
Log likelihood........................: -2709.88739581625

[Setting up likelihood functions]
Tree likelihood model..................: Homogeneous

[Parameter sanity check]
Initial log likelihood.................: -2709.88739581625

[Executing numerical parameters and topology optimization]
Message handler........................: none
Optimizator profiler...................: none
optimization.constrain_parameter is deprecated, use optimization.constrain_parameters instead!
