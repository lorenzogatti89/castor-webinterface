Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/a627eb68-30be-11e8-8e04-0a2b935a2bfe/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master bd8cbf58b22d6100c7751f18d9c016a708ab027e on date: 26 Mar 2018, 03:07:00                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: DNA
Allow gaps as extra character..........: no

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 13

[Preparing initial tree]
Initial tree...........................: distance
Initial tree method....................: LZ compression
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/a627eb68-30be-11e8-8e04-0a2b935a2bfe/best_tree.nwk
Output tree format ....................: Newick
Branch lengths.........................: Input

[Setting up substitution model]
Substitution model.....................: JC69
WARNING!!! Parameter rate_distribution not specified. Default used instead: Constant()
Distribution...........................: Constant
Number of classes......................: 1
- Category 0 (Pr = 1) rate.............: 1
Rate distribution......................: Constant
Number of classes......................: 1

[Computing the multi-sequence alignment]
ParameterNotFoundException: ParameterList::getParameter('name').(JC69.lambda)
