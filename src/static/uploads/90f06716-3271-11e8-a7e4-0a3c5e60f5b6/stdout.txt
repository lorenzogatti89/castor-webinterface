Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master 7b8a88927fb4e9cc37355b718ef068adc03efb78 on date: 27 Mar 2018, 12:32:51                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: DNA
Allow gaps as extra character..........: yes

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 13

[Preparing initial tree]
Initial tree...........................: user
Input tree file .......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/real_real_shh300_nt.tree.nwk
Input tree format .....................: Newick
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/best_tree.nwk
Output tree format ....................: Newick
Branch lengths.........................: Input

[Setting up substitution model]
Substitution model.....................: GTR+PIP
PIP.GTR.a..............................: 1
PIP.GTR.b..............................: 1
PIP.GTR.c..............................: 1
PIP.GTR.d..............................: 1
PIP.GTR.e..............................: 1
PIP.GTR.theta..........................: 0.5
PIP.GTR.theta1.........................: 0.5
PIP.GTR.theta2.........................: 0.5
PIP.lambda.............................: 0.1
PIP.mu.................................: 0.2
WARNING!!! Parameter rate_distribution not specified. Default used instead: Constant()
Distribution...........................: Constant
Number of classes......................: 1
- Category 0 (Pr = 1) rate.............: 1
Rate distribution......................: Constant
Number of classes......................: 1

[Computing the multi-sequence alignment]
[>                                     ]   4%[>>>                                   ]   8%[>>>>                                  ]  12%[>>>>>>                                ]  16%[>>>>>>>                               ]  20%[>>>>>>>>>                             ]  24%[>>>>>>>>>>                            ]  28%[>>>>>>>>>>>>                          ]  32%[>>>>>>>>>>>>>                         ]  36%[>>>>>>>>>>>>>>>                       ]  40%[>>>>>>>>>>>>>>>>                      ]  44%[>>>>>>>>>>>>>>>>>>                    ]  48%[>>>>>>>>>>>>>>>>>>>                   ]  52%[>>>>>>>>>>>>>>>>>>>>>                 ]  56%[>>>>>>>>>>>>>>>>>>>>>>                ]  60%[>>>>>>>>>>>>>>>>>>>>>>>>              ]  64%[>>>>>>>>>>>>>>>>>>>>>>>>>             ]  68%[>>>>>>>>>>>>>>>>>>>>>>>>>>>           ]  72%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>          ]  76%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        ]  80%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>       ]  84%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     ]  88%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    ]  92%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ]  96%[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] 100%
Log likelihood........................: -3364.82550347713

[Setting up likelihood functions]
Tree likelihood model..................: Homogeneous

[Parameter sanity check]
Initial log likelihood.................: -3364.82550347714

[Executing numerical parameters and topology optimization]
Message handler........................: none
Optimizator profiler...................: none
Max # ML evaluations...................: 500
Tolerance..............................: 0.001
Optimize topology......................: yes
Optimization method....................: ND-BFGS
Algorithm used for derivable parameters: BFGS
Reparametrization......................: no
Molecular clock........................: None
Optimizing... / 1
Branch length parameters
Optimizing... / 1Optimizing... - 4Optimizing... \ 7Optimizing... - 10Optimizing... / 13Optimizing... - 16Optimizing... \ 19Optimizing... - 22Optimizing... / 25Optimizing... - 28Optimizing... \ 31Optimizing... - 34Optimizing... / 37Optimizing... - 40Optimizing... \ 43Optimizing... - 46Optimizing... / 49Optimizing... - 52Optimizing... \ 55Optimizing... - 58Optimizing... / 61

Rate & model distribution parameters
Optimizing... / 1Optimizing... / 5Optimizing... / 9Optimizing... / 13Optimizing... / 17Optimizing... / 21Optimizing... / 25Optimizing... / 29Optimizing... / 33Optimizing... / 37Optimizing... / 41Optimizing... / 45Optimizing... / 49Optimizing... / 53Optimizing... / 57Optimizing... / 61Optimizing... / 65Optimizing... / 69Optimizing... / 73Optimizing... / 77Optimizing... / 81Optimizing... / 85Optimizing... / 89Optimizing... / 93Optimizing... / 97Optimizing... / 101Optimizing... / 105Optimizing... / 109Optimizing... / 113Optimizing... / 117Optimizing... / 121Optimizing... / 125Optimizing... / 129Optimizing... / 133Optimizing... / 137Optimizing... / 141Optimizing... / 145Optimizing... / 149Optimizing... / 153Optimizing... / 157Optimizing... / 161Optimizing... / 165Optimizing... / 169Optimizing... / 173Optimizing... / 177Optimizing... / 181Optimizing... / 185Optimizing... / 189Optimizing... / 193Optimizing... / 197Optimizing... / 201Optimizing... / 205Optimizing... / 209Optimizing... / 213Optimizing... - 216Optimizing... - 220Optimizing... - 224Optimizing... - 228Optimizing... - 232Optimizing... - 236Optimizing... - 240Optimizing... - 244Optimizing... - 248Optimizing... - 252Optimizing... - 256Optimizing... - 260Optimizing... - 264Optimizing... - 268Optimizing... - 272Optimizing... - 276Optimizing... - 280Optimizing... \ 283Optimizing... - 286Optimizing... / 289Optimizing... - 292Optimizing... \ 295Optimizing... - 298Optimizing... / 301Optimizing... - 304Optimizing... \ 307Optimizing... - 310Optimizing... / 313Optimizing... - 316Optimizing... \ 319Optimizing... - 322Optimizing... / 325Optimizing... - 328Optimizing... \ 331Optimizing... - 334Optimizing... / 337Optimizing... - 340Optimizing... \ 343Optimizing... - 346Optimizing... / 349Optimizing... - 352Optimizing... \ 355
Optimizing... - 424
Branch length parameters
Optimizing... / 1Optimizing... - 4Optimizing... \ 7Optimizing... - 10Optimizing... / 13Optimizing... - 16Optimizing... \ 19Optimizing... - 22Optimizing... / 25Optimizing... - 28Optimizing... \ 31Optimizing... - 34Optimizing... / 37Optimizing... - 40Optimizing... \ 43Optimizing... - 46Optimizing... / 49Optimizing... - 52Optimizing... \ 55Optimizing... - 58Optimizing... / 61Optimizing... - 64Optimizing... \ 67Optimizing... - 70

Rate & model distribution parameters
Optimizing... / 1Optimizing... / 9Optimizing... - 14Optimizing... \ 19Optimizing... - 24Optimizing... / 29Optimizing... - 34


WARNING!!! Parameter optimization.topology.algorithm.maxcycles not specified. Default used instead: 1
WARNING!!! Parameter optimization.topology.algorithm.hillclimbing.startnodes not specified. Default used instead: 1
Topology optimization | Algorithm......: greedy
Topology optimization | Moves class....: best-search
Topology optimization | BrLen optimisat: BFGS
Topology optimization | Max # cycles...: 1
Tree-search cycle #001 | Testing 220 tree rearrangements.
node NM_204169.1_Gallus_gallus[>                                     ]   2%node NM_204169.1_Gallus_gallus[>>                                    ]   5%node NM_204169.1_Gallus_gallus[>>>                                   ]   8%node NM_001033969.1_Monodelphis_dom[>>>>                                  ]  10%node NM_001033969.1_Monodelphis_dom[>>>>>                                 ]  13%node NM_001033969.1_Monodelphis_dom[>>>>>>                                ]  16%node V22[>>>>>>>                               ]  19%node V22[>>>>>>>>                              ]  21%node V22[>>>>>>>>>                             ]  24%node V4[>>>>>>>>>>                            ]  27%node V4[>>>>>>>>>>>                           ]  30%node NM_009764.3_Mus_musculus[>>>>>>>>>>>>                          ]  32%node NM_009764.3_Mus_musculus[>>>>>>>>>>>>>                         ]  35%node NM_009764.3_Mus_musculus[>>>>>>>>>>>>>>                        ]  38%node NM_012514.1_Rattus_norvegicus[>>>>>>>>>>>>>>>                       ]  40%node NM_012514.1_Rattus_norvegicus[>>>>>>>>>>>>>>>>                      ]  43%node NM_012514.1_Rattus_norvegicus[>>>>>>>>>>>>>>>>>                     ]  46%node V21[>>>>>>>>>>>>>>>>>>                    ]  49%node V21[>>>>>>>>>>>>>>>>>>>                   ]  51%node V7[>>>>>>>>>>>>>>>>>>>>                  ]  54%node V7[>>>>>>>>>>>>>>>>>>>>>                 ]  57%node NM_178573.1_Bos_taurus[>>>>>>>>>>>>>>>>>>>>>>                ]  60%node NM_178573.1_Bos_taurus[>>>>>>>>>>>>>>>>>>>>>>>               ]  62%node NM_001013416.1_Canis_lupus[>>>>>>>>>>>>>>>>>>>>>>>>              ]  65%node NM_001013416.1_Canis_lupus[>>>>>>>>>>>>>>>>>>>>>>>>>             ]  68%node NM_001013416.1_Canis_lupus[>>>>>>>>>>>>>>>>>>>>>>>>>>            ]  70%node V20[>>>>>>>>>>>>>>>>>>>>>>>>>>>           ]  73%node V14[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>         ]  76%node NM_001308668.1_Nomascus_leucog[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        ]  79%node NM_001308668.1_Nomascus_leucog[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>       ]  81%node V13[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>      ]  84%node NM_007294.3_Homo_sapiens[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     ]  87%node V12[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    ]  90%node NM_001301758.1_Pan_paniscus[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   ]  92%node NM_001045493.1_Pan_troglodytes[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ]  95%node NM_001302103.1_Papio_anubis[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ]  98%node NM_001114949.1_Macaca_mulatta[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] 100%

Tree-search cycle #001 | [NNI.194] New likelihood: -1452.11880277
Optimising 10 branches.................: Optimizing... / 1Done.

Final optimization step...............: bfgs
Optimizing... / 1
Branch length parameters
Optimizing... / 1

Rate & model distribution parameters
Optimizing... / 1
Optimizing... / 13
Branch length parameters
Optimizing... / 1

Rate & model distribution parameters
Optimizing... / 1
Optimizing... - 28
Branch length parameters
Optimizing... / 1

Rate & model distribution parameters
Optimizing... / 1



Performed.............................: 40 function evaluations.
Likelihood after num/top optimisation..: -1452.11437221045
MSA optimization.......................: no
Output alignment to file...............: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/best_alignment.fa
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/best_tree.nwk
Output tree format ....................: Newick
Log likelihood.........................: -1452.11437221045
PIP.GTR.a..............................: 1.05326
PIP.GTR.b..............................: 0.214341
PIP.GTR.c..............................: 0.248218
PIP.GTR.d..............................: 0.341702
PIP.GTR.e..............................: 0.310027
PIP.GTR.theta..........................: 0.402261
PIP.GTR.theta1.........................: 0.53254
PIP.GTR.theta2.........................: 0.56534
PIP.lambda.............................: 4.84243
PIP.mu.................................: 0.0145812
WARNING!!! This parameter has a value close to the boundary: BrLen8(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen10(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen12(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen15(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen16(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen17(1e-06).
WARNING!!! This parameter has a value close to the boundary: BrLen18(1e-06).
WARNING!!! Parameter output.estimates.alias not specified. Default used instead: 1
Output estimates to file...............: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/90f06716-3271-11e8-a7e4-0a3c5e60f5b6/best_estimates.log
Total execution time: 0.000000d, 0.000000h, 2.000000m, 59.000000s.
