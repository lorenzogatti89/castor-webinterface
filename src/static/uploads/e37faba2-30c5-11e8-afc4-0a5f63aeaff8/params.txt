alphabet=DNA
input.sequence.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/brca1_300nt.fa
alignment=true
input.sequence.sites_to_use=all
init.tree=distance
init.distance.method=bionj
init.distance.matrix.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/dm.txt
model=PIP(model=JC69, lambda=0.1, mu=0.2)
optimization=ND-Brent(derivatives=Newton)
optimization.final=bfgs
optimization.ignore_parameters=BrLen,Model
optimization.topology=false
optimization.topology.algorithm=greedy
optimization.topology.algorithm.operations=nni-search
optimization.topology.brlen_optimization=Brent
output.tree.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/best_tree.nwk
output.msa.file=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/best_alignment.fa
output.estimates=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/best_estimates.log
output.infos=/var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/e37faba2-30c5-11e8-afc4-0a5f63aeaff8/info.log
