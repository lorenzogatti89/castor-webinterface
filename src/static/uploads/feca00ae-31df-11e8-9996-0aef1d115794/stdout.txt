Parsing file /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/feca00ae-31df-11e8-9996-0aef1d115794/params.txt for options.
*****************************************************************************************************************************************
* miniJATI 0.1.1 by Lorenzo Gatti & Massimo Maiolo                                                                                      *
* Build on commit: refs/heads/master 7b8a88927fb4e9cc37355b718ef068adc03efb78 on date: 27 Mar 2018, 12:32:51                           *
*****************************************************************************************************************************************
Execution started on:
Log files location.....................: current execution path
WARNING!!! Parameter exec_numthreads not specified. Default used instead: 1
Alphabet...............................: Proteic
Allow gaps as extra character..........: yes

[Preparing input data]
Aligned sequences......................: yes
Number of sequences....................: 12

[Preparing initial tree]
Initial tree...........................: distance
Initial tree method....................: LZ compression
Output tree file ......................: /var/www/html/efs/projects/jati_webinterface/minijati_webserver/static/uploads/feca00ae-31df-11e8-9996-0aef1d115794/best_tree.nwk
Output tree format ....................: Newick
Branch lengths.........................: Input

[Setting up substitution model]
Model 'JC69lambda=0.1' is unknown, or does not fit proteic alphabet.
