#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""

# Import section (built-in modules|third-party modules)
import os
import signal
import zipfile
from datetime import date, datetime
import sqlite3
from django.conf import settings
from subprocess import Popen, PIPE, TimeoutExpired

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from wsgiref.util import FileWrapper
# Authorship information

__project__ = 'src'
__product__ = 'backend.py'
__editor__ = 'PyCharm'
__author__ = 'lorenzogatti'
__copyright__ = "Copyright 2018, Lorenzo Gatti"
__credits__ = ["Lorenzo Gatti"]
__license__ = "GPL"
__date__ = '23.03.18'
__version__ = "1.0"
__maintainer__ = "Lorenzo Gatti"
__email__ = "lg@lorenzogatti.me"
__status__ = "Development"


# Main code

def computeDistanceMatrix(sessionPath, sequencefile):
    command = settings.EXECPATHS["path_DECAFPLUSPI"] + " -f " + sessionPath + "/" + sequencefile + " -o " + sessionPath + "/dm.txt";
    print(command)
    process = Popen(command, shell=True, stdout=PIPE)

    try:
        outs, errs = process.communicate(timeout=15)
    except TimeoutExpired:
        process.kill()


def compileParameterFile(sessionPath, map):
    filename = sessionPath + "/params.txt"
    with open(filename, 'w') as f:

        if (map['inputdata_alphabet'] != ""):
            f.write("alphabet=%s\n" % map['inputdata_alphabet'])

        if (map['inputdata_filename'] != ""):
            f.write("input.sequence.file=%s\n" % str(sessionPath + '/' + map['inputdata_filename'].name))

        if (map['alignment_execute'] is not None):
            if (map['alignment_execute'] == "yes"):
                f.write("alignment=%s\n" % "true")
            else:
                f.write("alignment=%s\n" % "false")

        if (map['alignment_profiler_filename'] != ''):
            f.write(" alignment.profiler=%s\n" % str(sessionPath + '/' + map['alignment_profiler_filename']))

        if (map['sites_settoconsider'] != ""):
            f.write("input.sequence.sites_to_use=%s\n" % map['sites_settoconsider'])

        if (map['sites_removestopcodons'] is not None):
            if (map['sites_removestopcodons'] == "yes"):
                f.write("input.sequence.remove_stop_codons=%s\n" % "true")

        if (map['site_gapsproportion'] != ''):
            f.write("input.sequence.max_gap_allowed=%s\n" % map['site_gapsproportion'])

        if (map['site_unresolvedproportion'] != ''):
            f.write("input.sequence.max_unresolved_allowed=%s\n" % map['site_unresolvedproportion'])

        if (map['sites_manualselection'] != ''):
            f.write("input.site.selection=%s\n" % map['sites_manualselection'])

        if (map['inittree_method'] != ''):
            f.write("init.tree=%s\n" % map['inittree_method'])
            if (map['inittree_method'] == "distance"):
                if (map['inittree_method_distance_alg'] != ''):
                    f.write("init.distance.method=%s\n" % map['inittree_method_distance_alg'])
            elif (map['inittree_method'] == "user"):
                if (map['inittree_method_user_filename'] is not None):
                    f.write("input.tree.file=%s\n" % str(sessionPath + '/' + map['inittree_method_user_filename'].name))
            else:
                if (map['inittree_method_random_bl'] != ''):
                    f.write("init.brlen.method=%s\n" % map['inittree_method_random_bl'])

        if (map['distancematrix_execute'] is not None):
            if (map['distancematrix_execute'] == 'yes'):
                callProgram = True
                computeDistanceMatrix(sessionPath, map['inputdata_filename'].name)
                f.write("init.distance.matrix.file=%s\n" % str(sessionPath + '/dm.txt'))
            else:
                if (map['inputdata_distmatrix_filename'] is not None):
                    f.write("init.distance.matrix.file=%s\n" % str(sessionPath + '/' + map['inputdata_distmatrix_filename'].name))

        if (map['submodel_model_desc'] is not None):
            f.write("model=%s\n" % map['submodel_model_desc'])

        if (map['submodel_asvr_desc'] != ''):
            f.write("rate_distribution=%s\n" % map['submodel_asvr_desc'])

        if (map['numopt_method'] != ''):
            if (map['numopt_method_bl'] != ''):
                f.write("optimization=%s(derivatives=%s)\n" % (map['numopt_method'], map['numopt_method_bl']))
            else:
                f.write("optimization=%s\n" % (map['numopt_method']))

        if (map['numopt_method_final'] != ''):
            f.write("optimization.final=%s\n" % map['numopt_method_final'])

        if (map['opt_reparametrization'] is not None):
            if (map['opt_reparametrization'] == "yes"):
                f.write("optimization.reparametrization=%s\n" % "true")

        if (map['optimization_profiler_filename'] != ''):
            f.write("optimization.profiler=%s\n" % str(sessionPath + '/' + map['optimization_profiler_filename']))

        if (map['optimization_messagehandler_filename'] != ''):
            f.write("optimization.message_handler=%s\n" % str(sessionPath + '/' + map['optimization_messagehandler_filename']))

        if (map['opt_eval_likelihoodprecision'] is not None):
            f.write("optimization.tolerance=%s\n" % map['opt_eval_likelihoodprecision'])

        if (map['opt_eval_maxevaluations'] is not None):
            f.write("optimization.max_number_f_eval=%s\n" % map['opt_eval_maxevaluations'])

        if (map['opt_parameter_ignore'] != ''):
            f.write("optimization.ignore_parameters=%s\n" % map['opt_parameter_ignore'])

        if (map['opt_parameter_constraints'] != ''):
            f.write("optimization.constrain_parameter=%s\n" % map['opt_parameter_constraints'])

        if (map['opt_topology_execute'] != ""):
            if (map['opt_topology_execute'] == "yes"):
                f.write("optimization.topology=%s\n" % "true")
            else:
                f.write("optimization.topology=%s\n" % "false")

        if (map['opt_topology_algorithm_desc'] != ''):
            f.write("optimization.topology.algorithm=%s\n" % map['opt_topology_algorithm_desc'])

        if (map['opt_topology_rearrangments'] != ''):
            f.write("optimization.topology.algorithm.operations=%s\n" % map['opt_topology_rearrangments'])

        if (map['opttop_method_bl'] != ''):
            f.write("optimization.topology.brlen_optimization=%s\n" % map['opttop_method_bl'])

        if (map['opttop_maxiterations'] is not None):
            f.write("optimization.topology.algorithm.maxcycles=%s\n" % map['opttop_maxiterations'])

        if (map['opt_topology_algo_hillclimbing_startnodes'] is not None):
            f.write("optimization.topology.algorithm.hillclimbing.startnodes=%s\n" % map['opt_topology_algo_hillclimbing_startnodes'])

        if (map['output_tree_filename'] != ''):
            f.write("output.tree.file=%s\n" % str(sessionPath + '/' + map['output_tree_filename']))
        else:
            map['output_tree_filename'] = "best_tree.nwk"
            f.write("output.tree.file=%s\n" % str(sessionPath + '/' + map['output_tree_filename']))

        if (map['output_alignment_filename'] != ''):
            f.write("output.msa.file=%s\n" % str(sessionPath + '/' + map['output_alignment_filename']))
        else:
            map['output_alignment_filename'] = "best_alignment.fa"
            f.write("output.msa.file=%s\n" % str(sessionPath + '/' + map['output_alignment_filename']))

        if (map['output_estimates_filename'] != ''):
            f.write("output.estimates=%s\n" % str(sessionPath + '/' + map['output_estimates_filename']))
        else:
            map['output_estimates_filename'] = "best_estimates.log"
            f.write("output.estimates=%s\n" % str(sessionPath + '/' + map['output_estimates_filename']))

        if (map['output_info_filename'] != ''):
            f.write("output.infos=%s\n" % str(sessionPath + '/' + map['output_info_filename']))
        else:
            map['output_info_filename'] = "info.log"
            f.write("output.infos=%s\n" % str(sessionPath + '/' + map['output_info_filename']))


def submitAnalysisToExecutor(sessionPath):
    command = settings.EXECPATHS["path_miniJATI"]
    params = str(" params=\"" + sessionPath + "/params.txt\" &> " + sessionPath + "/stdout.txt")

    proc = Popen(command + params, shell=True)

    return proc.pid


def plotResults(sessionPath, sessionid, mapParams):
    result_render = sessionPath + "/results.png"
    alignment = sessionPath + "/" + mapParams['output_alignment_filename']
    tree = sessionPath + "/" + mapParams['output_tree_filename']

    view_command = settings.EXECPATHS["path_XFVF"] + ' ' + settings.EXECPATHS["path_ETE3"] + " view -t " + tree + " --alg " + alignment + " -i " + result_render + " --alg_type=fullseq --Ir 300 --sbl"
    print(view_command)
    process = Popen(view_command, shell=True, stdout=PIPE)

    try:
        outs, errs = process.communicate(timeout=15)
    except TimeoutExpired:
        process.kill()

    return "uploads/" + str(sessionid) + "/results.png"


def zipdir(path, ziph):
    # ziph is zipfile handle
    abs_src = os.path.abspath(path)
    for root, dirs, files in os.walk(path):
        for file in files:
            absname = os.path.abspath(os.path.join(root, file))
            arcname = absname[len(abs_src) + 1:]
            # ziph.write(os.path.join(root, file))
            ziph.write(absname, arcname)


def makeAnalysisPackage(basepath, sessionid):
    # try:
    # os.remove(basepath+"/"+sessionid+"/param.pkl")
    # except FileNotFoundError:
    #    print('Nothing to do')

    zipf = zipfile.ZipFile(basepath + "/" + sessionid + "_pack.zip", 'w', zipfile.ZIP_DEFLATED)
    zipdir(basepath + "/" + sessionid + "/", zipf)
    zipf.close()

    # os.rmdir(basepath+"/"+sessionid)

    return "uploads/" + sessionid + "_pack.zip"



def addAnalysis(sessionid, pid, username, machineid):

    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])

    now = datetime.now()
    task = (str(sessionid), now, '', pid, username, machineid, '')

    try:
        sql = ''' INSERT INTO sessions(uuid,start_time,end_time, pid, username, exec_machine, exec_desc) VALUES(?,?,?,?,?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, task)
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')




def updateAnalysis(sessionid, statusid):
    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])

    now = datetime.now()
    task = (now, statusid, str(sessionid))

    try:
        sql = ''' UPDATE sessions SET end_time = ?, exec_status = ? WHERE uuid = ? AND end_time = '' '''
        cur = conn.cursor()
        cur.execute(sql, task)
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')


def currentPendingAnalyses():
    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])
    sql = "SELECT Count(*) FROM sessions WHERE end_time = ''"
    records = 0

    try:
        cur = conn.cursor()
        cur.execute(sql)
        records = cur.fetchone()[0]
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')

    return records


def isProcessRunning(pid):
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True



def getPIDfromUUID(sessionid):
    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])
    uuid = str(sessionid)
    sql = "SELECT pid FROM sessions WHERE uuid = \"" + uuid + "\""
    pid = ""

    try:
        cur = conn.cursor()
        cur.execute(sql)
        pid = cur.fetchone()[0]
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')

    return pid

def killRunningSession(request, session_id):
    """ Check For the existence of a unix pid. """

    pid = int(getPIDfromUUID(session_id))
    status = True
    if pid:
        try:
            os.kill(pid, signal.SIGKILL)
        except OSError:
            status = False
        else:
            updateAnalysis(session_id, 1)
    return HttpResponseRedirect("/user/")


def packAnalysis(request, session_id):

    urlpackage = makeAnalysisPackage(settings.MEDIA_ROOT, str(session_id))
    filepath=settings.MEDIA_ROOT+"/"+urlpackage.split('/')[1]
    file = open(filepath, 'rb').read()
    response = HttpResponse(file, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename='+urlpackage.split('/')[1]

    return response

def clearUserCompletedProcesses(request):

    conn = sqlite3.connect(settings.DATABASES['default']['NAME'])
    sql = "DELETE FROM sessions WHERE username = '" + request.user.username + "' AND end_time != ''"
    print(sql)

    try:
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        print('ERROR:')