#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""

# Import section (built-in modules|third-party modules)
from django.apps import AppConfig

# Authorship information

__project__ = 'src'
__product__ = 'apps'
__editor__ = 'PyCharm'
__author__ = 'lorenzogatti'
__copyright__ = "Copyright 2018, Lorenzo Gatti"
__credits__ = ["Lorenzo Gatti"]
__license__ = "GPL"
__date__ = '21.03.18'
__version__ = "1.0"
__maintainer__ = "Lorenzo Gatti"
__email__ = "lg@lorenzogatti.me"
__status__ = "Development"


# Main code
class CastorWebserverConfig(AppConfig):
    name = 'castor_webinterface'
    verbose_name = 'Castor - Indel-aware phylogenetic inference web interface'
