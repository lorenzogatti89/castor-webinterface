<?php
// Start session
session_start();

$curdir = realpath(dirname(__FILE__));


function disable_ob() {
    // Turn off output buffering
    ini_set('output_buffering', 'off');
    // Turn off PHP output compression
    ini_set('zlib.output_compression', false);
    // Implicitly flush the buffer(s)
    ini_set('implicit_flush', true);
    ob_implicit_flush(true);
    // Clear, and turn off output buffering
    while (ob_get_level() > 0) {
        // Get the curent level
        $level = ob_get_level();
        // End the buffering
        ob_end_clean();
        // If the current level has not changed, abort
        if (ob_get_level() == $level) break;
    }
    // Disable apache output buffering/compression
    if (function_exists('apache_setenv')) {
        apache_setenv('no-gzip', '1');
        apache_setenv('dont-vary', '1');
    }
}

// Initialization variables
    $sep = " ";
    $arguments="";

    $alphabet="alphabet=";
    $input_sequences="input_sequences=";
    $input_tree="input_tree=";
    $input_distmatrix="distance_matrix=";
    $exec_alignment="alignment=";
    $lkmove="lkmove=";
    $optim_topology="optim_topology=";
    $model_substitution="model_substitution=";
    $model_indels="model_indels=";
    $model_setfreqsfromdata="model_setfreqsfromdata=";
    $model_pip_lambda="model_pip_lambda=";
    $model_pip_mu="model_pip_mu=";
    $optim_method_description="optim_method_description=";
    $optim_topology_algorithm="optim_topology_algorithm=";
    $optim_topology_operations="optim_topology_operations=";
    $optim_topology_maxcycles="optim_topology_maxcycles=";
    $optim_alignment_maxcycles="optim_alignment_maxcycles=";
    $output_file_tree="output_file_tree=";
    $output_file_msa="output_file_msa=";

    // Check if the form was submitted
    //if($_SERVER["REQUEST_METHOD"] == "POST"){
        // Get values from form

    $in_alphabet=$_POST['alphabet'];
    //$in_input_sequences=$_POST['input_sequences'];
    //$in_input_tree=$_POST['input_tree'];
    //$in_input_distmatrix=$_POST['input_distmatrix'];
    $in_exec_alignment=$_POST['exec_alignment'];
    $in_lkmove=$_POST['lkmove'];
    $in_optim_topology=$_POST['opt_tree_method'];
    $in_optim_alignment=$_POST['opt_align_method'];
    $in_model_substitution=$_POST['model_substitution'];
    $in_model_indels=$_POST['model_indels'];
    $in_model_setfreqsfromdata=$_POST['model_setfreqsfromdata'];
    $in_model_pip_lambda=$_POST['model_pip_lambda'];
    $in_model_pip_mu=$_POST['model_pip_mu'];
    $in_optim_method_description=$_POST['optim_method_description'];
    $in_optim_topology_algorithm=$_POST['optim_topology_algorithm'];
    $in_optim_topology_operations=$_POST['optim_topology_operations'];
    $in_optim_topology_maxcycles=$_POST['optim_topology_maxcycles'];
    $in_optim_alignment_maxcycles=$_POST['opt_align_cycles'];
    //$lkmove=$_POST['output_file_tree']
    //$lkmove=$_POST['output_file_alignment']

    //Upload data
    if(isset($_FILES["input_alignment"])&& !empty($_FILES["input_alignment"]["name"])){
    // Check if file was uploaded without errors
    //if(isset($_FILES["input_alignment"]) && $_FILES["input_alignment"]["error"] == 0){
        //$allowed = array("fa" => "seq/fasta", "nwk" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");

        $in_input_sequences = $_FILES["input_alignment"]["name"];
        $newfilename = "upload/" .session_id()."_data_". $in_input_sequences;
        move_uploaded_file($_FILES["input_alignment"]["tmp_name"], $newfilename);

        $input_sequences.=$curdir."/".$newfilename;
        $arguments.=$input_sequences.$sep;

    } else{
        echo "Error: I could not find any sequence/alignment file: " . $_FILES["input_alignment"]["error"];
        exit(1);
    }

    //Upload tree
    if(isset($_FILES["input_tree"]) && !empty($_FILES["input_tree"]["name"])){
    //if(isset($_FILES["input_tree"]) && $_FILES["input_tree"]["error"] == 0){
        //$allowed = array("fa" => "seq/fasta", "nwk" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $in_input_tree = $_FILES["input_tree"]["name"];


        $newfilename = "upload/" .session_id()."_tree_". $in_input_tree;
        move_uploaded_file($_FILES["input_tree"]["tmp_name"], $newfilename);

        echo "\n\nI see a tree file\n";

        $input_tree.=$curdir."/".$newfilename;
        $arguments.=$input_tree.$sep;

    } else{
        //echo "Error: " . $_FILES["input_tree"]["error"];
    }




    /*
    if(isset($_FILES["input_distmatrix"]) && $_FILES["input_distmatrix"]["error"] == 0){
        //$allowed = array("fa" => "seq/fasta", "nwk" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $in_input_sequences = $_FILES["input_distmatrix"]["name"];
        move_uploaded_file($_FILES["input_distmatrix"]["tmp_name"], "upload/" .session_id(). $_FILES["input_distmatrix"]["name"]);
    } else{
        echo "Error: " . $_FILES["input_distmatrix"]["error"];
    }
    */

// If alignment is requested, sequences are provided and no guide tree is given, then compute the distance matrix
header("Content-type: text/plain");

// tell php to automatically flush after every output
// including lines of output produced by shell commands
disable_ob();

if($in_exec_alignment=='1' && !empty($_FILES["input_alignment"]["name"]) && empty($_FILES["input_tree"]["name"])){
    $dm_out =$curdir."/upload/".session_id()."_dm";
    $dm_command = "../source/decaf+py/compute-lz-distance.py -f ".$curdir."/upload/".session_id()."_data_". $in_input_sequences." -o ".$dm_out;
    echo "[COMPUTING DM] ".$dm_command."\n";
    $rem_dm_command="rm ".$dm_out;
    system($rem_dm_command, $retval);
    echo "removing old file status: ".$retval."\n";
    system($dm_command, $retval);
    echo "computing dm status: ".$retval."\n";
    sleep(5);
    $input_distmatrix.=$curdir."/upload/".session_id()."_dm";
    $arguments.=$input_distmatrix.$sep;
}

if(!empty($in_alphabet)){
    $alphabet.=$in_alphabet;
    $arguments.=$alphabet.$sep;
}

if($in_exec_alignment=='1'){
    $exec_alignment.="true";

}else{
    $exec_alignment.="false";

}
$arguments.=$exec_alignment.$sep;

if(!empty($in_lkmove)){
    $lkmove.=$in_lkmove;
    $arguments.=$lkmove.$sep;
}

if(!empty($in_optim_topology)){
    $optim_topology.=$in_optim_topology;
    $arguments.=$optim_topology.$sep;
}
if(!empty($in_model_substitution)){
    $model_substitution.=$in_model_substitution;
    $arguments.=$model_substitution.$sep;
}
if(!empty($in_model_indels)){
    $model_indels.="true";
    $model_pip_mu.=$in_model_pip_mu;
    $model_pip_lambda.=$in_model_pip_lambda;
    $arguments.=$model_pip_mu.$sep;
    $arguments.=$model_pip_lambda.$sep;
}else{
    $model_indels.="false";
}
$arguments.=$model_indels.$sep;
if(!empty($in_model_setfreqsfromdata)){
    $model_setfreqsfromdata.="true";
}else{
    $model_setfreqsfromdata.="false";
}
$arguments.=$model_setfreqsfromdata.$sep;

$optim_topology_algorithm.=$in_optim_topology_algorithm;
$optim_topology_operations.=$in_optim_topology_operations;
$optim_topology_maxcycles.=$in_optim_topology_maxcycles;

$arguments.=$optim_topology_algorithm.$sep.$optim_topology_operations.$sep.$optim_topology_maxcycles.$sep;


$output_file_msa.=$curdir."/"."upload/".session_id()."_alignment.fa";
$arguments.=$output_file_msa.$sep;
$output_file_tree.=$curdir."/"."upload/".session_id()."_tree.nwk";
$arguments.=$output_file_tree.$sep;



$binary = "../minijati/miniJATI";
echo "Exec: ".$binary."\n";
echo "Args: ".$arguments."\n";

//$arguments = "alphabet=DNA input_sequences=/Users/lorenzogatti/Documents/projects/minijati/data/MSA_fullA_5.fa lkmove=bothways optim_topology=full-search model_substitution=GTR model_indels=true model_setfreqsfromdata=false model_pip_lambda=0.2 model_pip_mu=0.1";

// get only stderr from console output
$extra = "2>&1 > /dev/null";
//$extra ="";

// compose commandline
$command = "export GLOG_v=0; ".$binary." ".$arguments." ".$extra;

// execute command and wait for its completion
system($command);

// execute ete to produce an image
sleep(1);
$result_rendering=$curdir."/"."upload/" .session_id()."_out.png";

system("rm $result_rendering");

$view_command = "xvfb-run /var/www/html/efs/projects/bin/anaconda_ete/bin/ete3 view -t ".$curdir."/"."upload/".session_id()."_tree.nwk"." --alg ".$curdir."/"."upload/".session_id()."_alignment.fa"." -i ".$result_rendering." --sbl --alg_type fullseq --resolution 300";
echo "[EXEC] ".$view_command."\n";
echo "[DONE] Execution is succesfully completed! I am rendering the results. Please wait...\n";


system($view_command);

$result_fileid = session_id()."_out.png";
$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

echo "[DONE] http://$host$uri/exec_results.php?file_id=$result_fileid\n";

ob_end_flush( );

header("Location: exec_results.php?file_id=$result_fileid");


exit;
